package hrytsenko.nasdaq.endpoint;

import hrytsenko.nasdaq.domain.data.Company;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Test;

import com.google.common.io.Resources;

public class NasdaqScraperTest {

    @Test
    public void testScrap() throws Exception {
        String content = Resources.toString(Resources.getResource("nasdaq/companies.csv"), StandardCharsets.UTF_8);

        String exchange = "NASDAQ";
        List<Company> companies = NasdaqScraper.scrapCompanies(exchange, content);
        companies.forEach(company -> Assert.assertEquals(exchange, company.getExchange()));

        List<String> symbols = companies.stream().map(company -> company.getSymbol()).collect(Collectors.toList());
        Assert.assertEquals(Arrays.asList("GOOG", "MSFT"), symbols);
    }

}
