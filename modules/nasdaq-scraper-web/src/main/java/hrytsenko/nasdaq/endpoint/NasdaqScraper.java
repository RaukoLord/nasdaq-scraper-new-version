package hrytsenko.nasdaq.endpoint;

import hrytsenko.nasdaq.domain.data.Company;
import hrytsenko.nasdaq.endpoint.data.NasdaqCompany;
import hrytsenko.nasdaq.error.ApplicationException;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

public class NasdaqScraper {

    private NasdaqScraper() {
    }

    public static List<Company> scrapCompanies(String exchange, String content) {
        try {
            CsvSchema schema = CsvSchema.builder().setUseHeader(true).build();
            CsvMapper mapper = new CsvMapper();
            ObjectReader reader = mapper.readerFor(NasdaqCompany.class).with(schema);

            return stream(reader.readValues(content)).map(NasdaqCompany::toCompany)
                    .peek(company -> company.setExchange(exchange)).collect(Collectors.toList());
        } catch (IOException exception) {
            throw new ApplicationException("Could not load companies from NASDAQ.", exception);
        }
    }

    private static Stream<NasdaqCompany> stream(Iterator<NasdaqCompany> companies) {
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(companies, Spliterator.ORDERED), false);
    }

}
