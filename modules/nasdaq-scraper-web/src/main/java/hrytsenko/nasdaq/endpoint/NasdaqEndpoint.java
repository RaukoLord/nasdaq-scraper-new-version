package hrytsenko.nasdaq.endpoint;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.google.common.io.Resources;

import hrytsenko.nasdaq.domain.data.Company;
import hrytsenko.nasdaq.error.ApplicationException;
import hrytsenko.nasdaq.system.SettingsService;

@Stateless
public class NasdaqEndpoint {

    @Inject
    private SettingsService settingsService;

    public List<Company> downloadCompanies(String exchange) {
        try {
            String link = settingsService.getLinkForDownloadByExchange().replace("{exchange}", exchange);
            String content = Resources.toString(new URL(link), StandardCharsets.UTF_8);
            return NasdaqScraper.scrapCompanies(exchange, content);
        } catch (IOException exception) {
            throw new ApplicationException(String.format("Could not load data for %s.", exchange), exception);
        }
    }

}
